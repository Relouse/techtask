﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NavMeshAgent))]
public class ZombieMovementBeheviour : MonoBehaviour {

    [SerializeField]
    private float minMovementSpeed, maxMovementSpeed;//Минимальная и макс скорости передвижения
    private NavMeshAgent navMeshAgent;//ссылка
    private Transform target;
    private int maxAvoidancePriority = 50;//Приоритет navmesh
    protected Animator anim;
    protected bool moving;//двигается ли сейчас зомби
    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
    }
    void Start()
    {
        target = GameObject.FindWithTag("Player").transform;
        navMeshAgent.speed = Random.Range(minMovementSpeed, maxMovementSpeed);
        navMeshAgent.avoidancePriority = Random.Range(0, maxAvoidancePriority);
        InvokeRepeating("SetTarget", 0, 1);//Чтобы не вызывать в каждом кадре метод смены дистанции
    }
    void SetTarget()
    {
        navMeshAgent.SetDestination(target.position);
        moving = true;
    }
    public void StopChasing()
    {
        navMeshAgent.Stop();
    }
}
