﻿using UnityEngine;
using System.Collections;

public class ZombieHealth : Health {

    private GameManager GM;

    void Start()
    {
        curHealth = maxHealth;
        GM = GameObject.FindObjectOfType<GameManager>();
    }
    public override void OnDeath()
    {
        GM.curZombiesCount--;//уменьшаем текущее кол-во зомби на уровне
        GM.SpawnNewZombie();
        GetComponent<Animator>().SetTrigger("Dead");
        GetComponent<ZombieMovementBeheviour>().StopChasing();//хватит преследовать игрока
        GetComponent<CapsuleCollider>().isTrigger = true;
        Destroy(gameObject,3);
        dead = true;
        GM.KilledZombieAmount++;
    }



}
