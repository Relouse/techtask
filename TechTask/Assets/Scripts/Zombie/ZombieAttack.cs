﻿using UnityEngine;
using System.Collections;

public class ZombieAttack : MonoBehaviour {


    [SerializeField]
    private float damage;

    void OnCollisionEnter(Collision col)
    {
        if(col.collider.CompareTag("Player"))
        {
            GetComponent<ZombieHealth>().OnDeath();
            col.collider.GetComponent<TankHealth>().TakeDamage(damage);
        }
    }
}
