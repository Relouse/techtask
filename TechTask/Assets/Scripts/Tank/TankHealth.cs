﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TankHealth : Health {

    [SerializeField]
    private Slider healthSlider;//Спрайт здоровья над танком
    private Color zeroHealthColor = Color.red, fullHealthColor = Color.green;//Цвет здоровья когда оно полное и когда пустое
    [SerializeField]
    private ParticleSystem tankExplosionPrefab;//Префаб взрыва танка
 
    void Awake()
    {
        thisTransform = transform;
    }

    void Start()
    {
        curHealth = maxHealth;
        RefreshUI();
    }
    public override void TakeDamage(float dam)
    {
        base.TakeDamage(dam);
        RefreshUI();
    }
    /// <summary>
    /// Обновления UI здоровья над танком
    /// </summary>
    void RefreshUI()
    {
        healthSlider.value = curHealth / maxHealth;
        healthSlider.fillRect.GetComponent<Image>().color = Color.Lerp(zeroHealthColor, fullHealthColor, curHealth / maxHealth);//создаем эффект изменения цвета в зависимости от кол-ва здоровья
    }
    /// <summary>
    /// Событие когда танк умер
    /// </summary>
    public override void OnDeath()
    {
        Instantiate(tankExplosionPrefab, thisTransform.position, Quaternion.identity);//создаем взрыв
        GetComponent<TankMovement>().enabled = false;
        GetComponent<WeaponsManager>().OnDeath();
        dead = true;//говорим что танк умер
        GameObject.FindObjectOfType<GameManager>().Lose();
    }
}
