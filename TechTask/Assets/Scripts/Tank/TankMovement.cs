﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AudioSource))]
public class TankMovement : MonoBehaviour
{
    [SerializeField]
    private float tankSpeed = 12f; // Скорость передвижения танка
    [SerializeField]
    private float tankTurnSpeed = 180f; // Скорость поворотов танка
    [SerializeField]
    private AudioClip tankEngineIdleSound; // Звук спокойствия танка
    [SerializeField]
    private AudioClip tankEngineMovingSound; // Звук движения танка
    [SerializeField]
    private float pitchRange = 0.2f; // Размах смены pitch у звука

    public AudioSource engineAudioSource; // Ссылка на компонент воспроизводящий звук
    private float originalPitch; // Стартовый pitch звука
    private Rigidbody rigid; // Ссылка на физику танка
    private float movementCurrentValue; // Текущее значения передвижения
    private float turnCurrentValue; // Текущее значение поворота
    


    private void Awake()
    {
        rigid = GetComponent<Rigidbody>();
    }

    
    private void Start()
    {
 
        // Сохраняем стартовый pitch
        originalPitch = engineAudioSource.pitch;
        engineAudioSource.clip = tankEngineIdleSound;
    }


    private void Update()
    {
        // получаем данные с axis
        movementCurrentValue = CrossPlatformInputManager.GetAxis("Vertical");
        turnCurrentValue = CrossPlatformInputManager.GetAxis("Horizontal");

        PlayEngineAudio();
    }

    /// <summary>
    /// Воспроизведение звука двигателя танка
    /// </summary>
    private void PlayEngineAudio()
    {
        //Если танк не двигается
        if (Mathf.Abs(movementCurrentValue) < 0.1f && Mathf.Abs(turnCurrentValue) < 0.1f)
        {
            //И звук = звуку передвижения
            if (engineAudioSource.clip == tankEngineMovingSound)
            {
                engineAudioSource.clip = tankEngineIdleSound;
                engineAudioSource.pitch = Random.Range(originalPitch - pitchRange, originalPitch + pitchRange);
                engineAudioSource.Play();
            }
        }
        else
        {
            if (engineAudioSource.clip == tankEngineIdleSound)
            {
                engineAudioSource.clip = tankEngineMovingSound;
                engineAudioSource.pitch = Random.Range(originalPitch - pitchRange, originalPitch + pitchRange);
                engineAudioSource.Play();
            }
        }
    }


    private void FixedUpdate()
    {
        Move();
        Turn();
    }

    /// <summary>
    /// Перемещение танка
    /// </summary>
    private void Move()
    {
        Vector3 movement = transform.forward * movementCurrentValue * tankSpeed * Time.deltaTime;

        rigid.MovePosition(rigid.position + movement);
    }

    /// <summary>
    /// Поворот танка
    /// </summary>
    private void Turn()
    {
        float turn = turnCurrentValue * tankTurnSpeed * Time.deltaTime;

        Quaternion turnRotation = Quaternion.Euler(0f, turn, 0f);

        rigid.MoveRotation(rigid.rotation * turnRotation);
    }
}
