﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    private static UIManager instance = null;
    public static UIManager Instance
    {
        get
        {
            return instance;
        }
    }
    [SerializeField]
    private Text killedZombieText;
    [SerializeField]
    private GameObject WinWindow,LoseWindow;
    void Awake()
    {
        if(instance)
        {
            DestroyImmediate(gameObject);
            return;
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
    }

    public void RefreshKilledZombies(string text)
    {
        killedZombieText.text = text;
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void Win()
    {
        WinWindow.SetActive(true);
        WinWindow.transform.parent.FindChild("GameMenu").gameObject.SetActive(false);
    }
    public void Lose(string progressText)
    {
        LoseWindow.SetActive(true);
        LoseWindow.transform.FindChild("Fon/Description").GetComponent<Text>().text = progressText;
        LoseWindow.transform.parent.FindChild("GameMenu").gameObject.SetActive(false);
    }

}
