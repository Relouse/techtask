﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    private static GameManager instance = null;
    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }
    private int maxZombiesCount = 10;
    [HideInInspector]
    public int curZombiesCount = 0;
    
    private Transform zombiesSpawnArea;
    [SerializeField]
    private GameObject[] zombiePrefabs;
    [HideInInspector]
    private int killedZombieAmount = 0, needZombieAmount = 30;
    private bool gameStarted = false;
    public int KilledZombieAmount
    {
        get
        {
            return killedZombieAmount;
        }
        set
        {
            killedZombieAmount = value;
            UI.RefreshKilledZombies(killedZombieAmount + "/" + needZombieAmount);
            if (killedZombieAmount == needZombieAmount) Win();
        }
    }
    private UIManager UI;
    void Awake()
    {
        if(instance)
        {
            DestroyImmediate(gameObject);
            return;
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start()
    {
        zombiesSpawnArea = GameObject.FindWithTag("ZombiesSpawnArea").transform;
       
        UI = GameObject.FindObjectOfType<UIManager>();
        KilledZombieAmount = 0;
        Time.timeScale = 0;
    }
    
    public void StartGame()
    {
        if (!gameStarted)
        {
            for (int i = 0; i < 10; i++)
            {
                SpawnNewZombie();
            }
            gameStarted = true;
        }
        ResumeGame();
    }
    public void PauseGame()
    {
        Time.timeScale = 0;
    }
    public void ResumeGame()
    {
        Time.timeScale = 1;
    }
    void Win()
    {
        PauseGame();
        UI.Win();
    }
    public void Lose()
    {
        PauseGame();
        UI.Lose("You killed: " + killedZombieAmount + "/" + needZombieAmount);
    }
    public void SpawnNewZombie()
    {
        if(curZombiesCount < maxZombiesCount)
        {
            GameObject point = new GameObject();
            point.transform.SetParent(zombiesSpawnArea);
            point.transform.localPosition = new Vector3(Random.Range(-5, 6) / 10f, 0, Random.Range(-5, 6) / 10f);//Промежуток спавна от -0.5 до 0.5

            Instantiate(zombiePrefabs[Random.Range(0, zombiePrefabs.Length)],point.transform.position, Quaternion.identity);
            Destroy(point);

            curZombiesCount++;
        }
    }
}
