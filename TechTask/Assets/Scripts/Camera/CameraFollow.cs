﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class CameraFollow : MonoBehaviour
{

    private Transform target;

    public float distance = 3;

    private float curHeight = 1;
    [SerializeField]
    private float minHeight = 0.5f, maxHeight = 12, changingHeightSpeed = 3f;

    // How much we 
    public float heightDamping = 2.0f;
    public float rotationDamping = 3.0f;

    void Start()
    {
        target = GameObject.FindWithTag("Player").transform;
    }
    void Update()
    {
        if (curHeight > maxHeight) curHeight = maxHeight;
        if (curHeight < minHeight) curHeight = minHeight;
        if (CrossPlatformInputManager.GetButton("CameraUp")) curHeight += Time.deltaTime * changingHeightSpeed;
        if (CrossPlatformInputManager.GetButton("CameraDown")) curHeight -= Time.deltaTime * changingHeightSpeed;
    }
    void LateUpdate()
    {
        if (!target)
            return;

        float wantedRotationAngle = target.eulerAngles.y;
        float wantedHeight = target.position.y + curHeight;
        float currentRotationAngle = transform.eulerAngles.y;
        float currentHeight = transform.position.y;

        currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);

        currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);

        Quaternion currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);

       
            transform.position = target.position;
            transform.position -= currentRotation * Vector3.forward * distance;

            transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);

            transform.LookAt(target);

    }

}
