﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Health : MonoBehaviour {

    protected float curHealth;//Текущее здоровье
    [SerializeField]
    protected float maxHealth = 100;//Максимальное здоровье, у каждого объекта будет свое, т.к. можно будет настроить в Inspector
    [SerializeField]
    [Range(0.1f,1)]
    private float protectionValue;//Защита
    
    protected Transform thisTransform;//для оптимизации
    protected bool dead = false;//Умер ли уже объект

    void Awake()
    {
        thisTransform = transform;
    }

    void Start()
    {
        curHealth = maxHealth;
    }

    
    /// <summary>
    /// Получение урона
    /// </summary>
    /// <param name="dam">Значение урона</param>
    public virtual void TakeDamage(float dam)
    {
        if (dead) return;//если танк итак мертв, выходим из метода
        curHealth -= dam * protectionValue;
        Debug.Log(curHealth);
        if (curHealth <= 0)
        {
            curHealth = 0;
            OnDeath();
        }
    }
    
    /// <summary>
    /// Событие когда объект умер
    /// </summary>
    public virtual void OnDeath()
    {
      
    }
}
