﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(AudioSource))]
public class Weapon : MonoBehaviour {

    public bool reloading = false;//Перезаряжается ли пушка
    protected bool canFire = true;//Можно ли стрелять (нужно, так как в некоторых случаях стрелять из пушки нельзя, даже если она перезаряжена)
    private AudioSource shootingSource;//Ссылка на звук
    [SerializeField]
    private float reloadingTime = 0.5f;//время перезарядки
    [SerializeField]
    private AudioClip fireClip;//Звук выстрела
    [SerializeField]
    private float backForceValue = 100;//Значение отдачи
    protected float fireMaxDistance = 100;//значение дальности выстрела (до куда может попасть танк выстрелом)

    [SerializeField]
    protected Transform firePoint;//точка откуда идет выстрел
    protected Transform thisTransform;//для оптимизации
    protected float damage;//Урон
    private int curPatrons;
    [SerializeField]
    private int  maxPatrons;
    [SerializeField]
    private Text patronsText;
    void Awake()
    {
        thisTransform = transform;
        shootingSource = GetComponent<AudioSource>();
        curPatrons = maxPatrons;
        RefreshUI();
    }
    /// <summary>
    /// Окончить перезарядку
    /// </summary>
    void EndReload()
    {
        reloading = false;
    }

	protected virtual void Update()
    {
        if (CrossPlatformInputManager.GetButton("Fire") && !reloading && canFire && curPatrons > 0 && Time.timeScale != 0)
        {
            curPatrons--;
            if (curPatrons < 0) curPatrons = 0;
            Fire();
            PlaySound(fireClip);
            ApplyBackForce();//Применяем отдачу к танку
            Invoke("EndReload", reloadingTime);//Через reloadingTime разрешаем снова стрелять
        }
    }

    /// <summary>
    /// Толкнуть танк противоположно выстрелу (отдача)
    /// </summary>
    void ApplyBackForce()
    {
        if (thisTransform.root.GetComponent<Rigidbody>())//если нашли rigidbody танка
            thisTransform.root.GetComponent<Rigidbody>().AddForce(-firePoint.forward * backForceValue, ForceMode.Force);//толкаем его

    }
    /// <summary>
    /// Функция атаки, которая для каждой пушки будет своя
    /// </summary>
    protected virtual void Fire()
    {
        RefreshUI();
    }
    public void RefreshUI()
    {
        patronsText.text = curPatrons + "/" + maxPatrons;
    }
    /// <summary>
    /// Воспроизведение звука
    /// </summary>
    /// <param name="clip"></param>
    void PlaySound(AudioClip clip)
    {
        shootingSource.clip = clip;
        shootingSource.Play();
    }
}
