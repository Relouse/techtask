﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;

public class WeaponsManager : MonoBehaviour {

    [SerializeField]
    private List<Weapon> weapons;//Список оружий

    void Start()
    {
        ChangeWeapon(0);//Сразу выбираем первую пушку
    }
    void Update()
    {
        if (CrossPlatformInputManager.GetButtonDown("Weapon1")) ChangeWeapon(0);
        if (CrossPlatformInputManager.GetButtonDown("Weapon2")) ChangeWeapon(1);
    }

    /// <summary>
    /// Смена пушки
    /// </summary>
    /// <param name="weaponID">Пушка, которую выбираем</param>
    void ChangeWeapon(int weaponID)
    {
        if (weapons[weaponID])
        {
            foreach (var item in weapons)
            {
                item.gameObject.SetActive(false);//выключаем все пушки
            }
            weapons[weaponID].gameObject.SetActive(true);//включаем выбранную
            weapons[weaponID].RefreshUI();//включаем выбранную
        }
    }
    /// <summary>
    /// Отключение всех пушек при смерти
    /// </summary>
    public void OnDeath()
    {
        foreach (var item in weapons)
        {
            item.enabled = false;
        }
    }
}
