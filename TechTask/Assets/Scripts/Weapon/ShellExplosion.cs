using UnityEngine;


    public class ShellExplosion : MonoBehaviour
    {
        [SerializeField]
        private LayerMask zombiesMask;//���� �����, ����� ���� ��� ������ �� ��������
        [SerializeField]
        private ParticleSystem explosionEffect;//������ �� ������ ������
        [SerializeField]
        private AudioSource explosionSource;//������ �� ���� ������� ������
        private float damage = 100f;//����

        public float Damage
        {
            set
            {
                damage = value;
            }
        }
        [SerializeField]
        private float explosionForce = 1000f;//���� ������
        private float lifeTime = 2f;//����� ����� �������
        [SerializeField]
        private float explosionRadius = 5f;//������ ���������
    private float bloodLifeTime = 0.2f;

        void Start()
        {
            //���������� ������ ����� �������� ����� �����
            Destroy(gameObject, lifeTime);
        }


        void OnTriggerEnter (Collider other)
        {
			//�������� ���� �������� � ������
            Collider[] zombies = Physics.OverlapSphere (transform.position, explosionRadius, zombiesMask);
        for (int i = 0; i < zombies.Length; i++)
        {


            ZombieHealth targetHealth = zombies[i].GetComponent<ZombieHealth>();

            if (!targetHealth)
                continue;


            float damage = CalculateDamage(zombies[i].transform.position);

            targetHealth.TakeDamage(damage);

            GameObject bloodEffect = Instantiate(Resources.Load("BloodEffect" + Random.Range(1, 5)), zombies[i].transform.position, Quaternion.identity) as GameObject;
            Destroy(bloodEffect, bloodLifeTime);
            bloodEffect.transform.SetParent(zombies[i].transform);
        }

            explosionEffect.transform.parent = null;

            explosionEffect.Play();

            explosionSource.Play();

            Destroy (explosionEffect.gameObject, explosionEffect.duration);

            Destroy (gameObject);
        }


        private float CalculateDamage (Vector3 targetPosition)
        {
            //�������� ������ �� ������ �� ����
            Vector3 explosionToTarget = targetPosition - transform.position;

            //�������� ����� �������
            float explosionDistance = explosionToTarget.magnitude;

            
            float relativeDistance = (explosionRadius - explosionDistance) / explosionRadius;

            //������������ ����
            float damage = relativeDistance * this.damage;

            damage = Mathf.Max (0f, damage);//������� ������������� ��������

            return damage;
        }
    }
