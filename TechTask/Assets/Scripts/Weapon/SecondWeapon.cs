﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class SecondWeapon : Weapon
{

    [SerializeField]
    private Rigidbody shellPrefab;                                
    [SerializeField]
    private float launchForce = 15f;//Сила выстрела  
    
    void Start()
    {
        damage = 100;
        canFire = true;
    }

    protected override void Update()
    {
        base.Update();
        canFire = Mathf.Abs(CrossPlatformInputManager.GetAxis("Vertical")) < 0.1f;
    }
    /// <summary>
    /// Переопределение функции атаки
    /// </summary>
    protected override void Fire()
    {
        base.Fire();
        reloading = true;
        Rigidbody shell = Instantiate(shellPrefab, firePoint.position, firePoint.rotation) as Rigidbody;//создаем снаряд
        shell.velocity = launchForce * firePoint.forward;//метаем снаряд
        shell.GetComponent<ShellExplosion>().Damage = damage;

    }
}
