﻿using UnityEngine;
using System.Collections;

public class FirstWeapon : Weapon {

    [SerializeField]
    private LineRenderer lineEffect;
    private float lineEffectLifeTime = 0.1f, decalLifeTime = 10f, bloodLifeTime = 0.2f;

    void Start()
    {
        damage = 50;
    }
    /// <summary>
    /// Переопределение функции атаки
    /// </summary>
    protected override void Fire()
    {
        base.Fire();
        reloading = true;

        RaycastHit hit;
        Vector3 forward = thisTransform.TransformDirection(Vector3.forward);
       
         
            if (Physics.Raycast(firePoint.position, forward, out hit, fireMaxDistance))
            {
                //Ставим луч
                LineRenderer lineEffectObj = Instantiate(lineEffect, firePoint.position, Quaternion.identity) as LineRenderer;
                lineEffectObj.SetPosition(0, firePoint.position);
                lineEffectObj.SetPosition(1, hit.point);
                Destroy(lineEffectObj.gameObject, lineEffectLifeTime);
            if (hit.collider.transform.root.CompareTag("Zombie"))
            {
                GameObject bloodEffect = Instantiate(Resources.Load("BloodEffect" + Random.Range(1, 5)), hit.point + (hit.normal * 0.025f), Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject;
                Destroy(bloodEffect, bloodLifeTime);
                bloodEffect.transform.SetParent(hit.collider.transform);
                hit.collider.transform.root.GetComponent<ZombieHealth>().TakeDamage(damage);
            }
            else
            {
                //Ставим декаль
                GameObject decal = Instantiate(Resources.Load("dirtMark" + Random.Range(1, 4)), hit.point + (hit.normal * 0.025f), Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject;
                Destroy(decal, decalLifeTime);
            }
            }
        
    }
}
